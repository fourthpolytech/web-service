package server.com.highTune;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.highTune.Article;
import com.highTune.ArticleNotFoundException;
import com.highTune.Catalogue;

public class CatalogueImplementation implements Catalogue{
	
	Map<String, Article> listArticle  = new HashMap<>();
	static CatalogueImplementation cata = new CatalogueImplementation();
	
	private CatalogueImplementation(){};
	
	public static CatalogueImplementation getInstance(){
		return cata;
	}
	
	/**
	 * Ajouter un article dans le catalogue
	 * @param cle
	 * @param article
	 */
	public void ajouterArticle(String cle, Article article){
		//TRAITER L'EXCEPTION SI CETTE EXISTE DEJA
		listArticle.put(cle, article);
	}
	
	/**
	 * Ajouter une liste d'article dans le catalogue
	 * @param cle
	 * @param article
	 */
	public void ajouterArticle(List<Article> article){
		for(Article a : article){
			listArticle.put(a.getCle(), a);
		}
	}
	

	/**
	 * Renvoie le nombre total d'article dans le systeme
	 */
	@Override
	public int nombreTotalArticle() throws RemoteException {
		return listArticle.size();
	}

	/**
	 * Descrption de l'artcle a
	 */
	@Override
	public String descriptionArticle(Article a){
		String s = "";
		if(a.getDateDispo() != null)
			s = a.getDateDispo().toString();
		
		return a.getCle() + "\n" + a.getDescrption() + "\n" + a.getPrix() + "\n" + s;
	}

	/**
	 * Recuperer un article à partir de sa cle
	 */
	@Override
	public Article getArticle(String cle) throws ArticleNotFoundException{
		Article art = listArticle.get(cle);
		if(art == null)
			throw new ArticleNotFoundException("Article not exist");
		return art;
	}

}
