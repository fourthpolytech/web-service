package server.com.highTune;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import com.highTune.Listenner;
import com.highTune.PanierClient;
import com.highTune.PoolPanier;

public class PoolPanierImpl<T extends Remote & PanierClient> implements PoolPanier<T>{
	
	private List<T> listObjet;
	private Map<T, Remote> objetsExporte = new HashMap<>();
	private int tailleMax;
	private int objDispo = 0;
	private Supplier<T> supplier;
	private List<Listenner> listener = new ArrayList<>();
	
	public PoolPanierImpl(Supplier<T> supplier, int tailleMax) {
		this.supplier = supplier;
		this.tailleMax = tailleMax;
	}

	@Override
	public void initialise() throws RemoteException {
		listObjet = new ArrayList<>();
		for(int i =0; i < tailleMax; i++){
			T p = (T) supplier.get();
			listObjet.add((T) p);
		}
	}

	@Override
	public int nbObjetDispo() throws RemoteException {
		return tailleMax - objDispo;
	}

	@Override
	public int tailleMax() throws RemoteException {
		return listObjet.size();
	}

	@Override
	public T getInstance() throws RemoteException {
		for(T t : listObjet){
			if(!((PanierClientImpl) t).affected){
				objDispo++;
				((PanierClientImpl) t).affected = true;
				//System.out.println(t.toString());
				T rm =  (T) UnicastRemoteObject.exportObject(t, 0);
				
				objetsExporte.put(rm, t);
				System.out.println(t.toString());
				
				notifier();
				return rm;
			}
		}
		notifier();
		return null;
		/*
		T t = listObjet.get(++objDispo); 
		listObjetInstancie.add(t);
		return (T) UnicastRemoteObject.exportObject(t, 0);
		*/
	}

	@Override
	public void release(T obj) throws RemoteException {
		if (obj == null)
			System.out.println("NULL");
		else 
			System.out.println(obj.toString());
		
		objDispo--;
		
		Remote r = objetsExporte.get(obj);
		System.out.println(r.toString());
		UnicastRemoteObject.unexportObject(r, true);
		
		//objetsExporte.remove(obj);
		obj.viderPanier();
		notifier();
	}
	
	public void addListnner(Listenner l){
		listener.add(l);
	}
	
	public void removeListenner(Listenner l){
		listener.remove(l);
	}
	

	public void notifier() throws RemoteException{
		for (Listenner l : listener){
			l.update();
		}
	}

	@Override
	public boolean isVide() {
		return listObjet.isEmpty();
	}
	

}
