package server.com.highTune;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import com.highTune.Article;
import com.highTune.Panier;
import com.highTune.PanierClient;

public class PanierClientImpl implements PanierClient{
	
	private Panier panier = new Panier();
	public boolean affected = false;

	@Override
	public PanierClient creerPanier() throws RemoteException {
		PanierClient pc = new PanierClientImpl();
		return (PanierClient) UnicastRemoteObject.exportObject(pc, 0);
	}

	@Override
	public void validerPanier() throws RemoteException {
		panier.convertirEnCommande();
	}

	@Override
	public void ajoutArticle(Article a) throws RemoteException {
		panier.ajouterArticle(a);
	}

	@Override
	public void supprimerArticle(Article a) throws RemoteException {
		panier.supprimerArticle(a);
	}

	@Override
	public void supprimerArticle(Article a, int quantite) throws RemoteException {
		panier.supprimerArticle(a, quantite);
	}

	@Override
	public float calculTotal() throws RemoteException {
		return panier.calculPanier();
	}

	@Override
	public void detruitePanier() throws RemoteException {
		UnicastRemoteObject.unexportObject(this, true);
	}

	@Override
	public void viderPanier() {
		panier.vider();
	}

	@Override
	public boolean isVide() {
		return panier.isVide();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (affected ? 1231 : 1237);
		result = prime * result + ((panier == null) ? 0 : panier.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PanierClientImpl other = (PanierClientImpl) obj;
		if (affected != other.affected)
			return false;
		if (panier == null) {
			if (other.panier != null)
				return false;
		} else if (!panier.equals(other.panier))
			return false;
		return true;
	}

}
