package server.com.highTune;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import com.highTune.Article;
import com.highTune.Catalogue;
import com.highTune.PanierClient;
import com.highTune.PoolPanier;

public class Server {
	public static void main(String[] args) throws RemoteException {
		
		CatalogueImplementation catalogueimp = CatalogueImplementation.getInstance();
		List<Article> list = new ArrayList<>();
		list.add(new Article("rock", "highway to hell", 2));
		list.add(new Article("jazz", "Take Five", 2));
		
		catalogueimp.ajouterArticle(list);
		Catalogue catalogue = (Catalogue)catalogueimp;
		
		Registry registry = LocateRegistry.createRegistry(1099);

		/*******   CATALOGUE    ******/
		Catalogue stubCat = (Catalogue) UnicastRemoteObject.exportObject(catalogue, 0);

		registry.rebind("catalogue", stubCat);
		System.out.println("Catalogue server is run");
		
		/*******   PANIER   ******/
		PanierClient panier = new PanierClientImpl();
		PanierClient stub2 = (PanierClient) UnicastRemoteObject.exportObject(panier, 0);
		
		registry.rebind("panier", stub2);
		//System.out.println("Panier server");
		
		/*******   POOL PANIER    ******/
		PoolPanier<PanierClient> poolP = new PoolPanierImpl<>(PanierClientImpl::new, 100);
		poolP.initialise();
		PoolPanier<PanierClient> stubPoolPanier = (PoolPanier<PanierClient>) UnicastRemoteObject.exportObject(poolP, 0);
		registry.rebind("poolPanier", stubPoolPanier);
		System.out.println("PoolPanier server");
	}
}
