package client.com.highTune;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import com.highTune.Article;
import com.highTune.ArticleNotFoundException;
import com.highTune.Builder;
import com.highTune.Catalogue;
import com.highTune.ListenerSeuil;
import com.highTune.Listenner;
import com.highTune.ListernerVide;
import com.highTune.Panier;
import com.highTune.PanierClient;
import com.highTune.PoolPanier;

import server.com.highTune.PanierClientImpl;


public class Client {

	public static void main(String[] args) throws RemoteException, NotBoundException, ArticleNotFoundException {
		Registry registry = LocateRegistry.getRegistry("localhost", 1099);
		
		

		Catalogue catalogue = (Catalogue) registry.lookup("catalogue");
		
		Article article = catalogue.getArticle("jazz");
		//System.out.println(article.getDescrption());
		
		PanierClient panier = (PanierClient) registry.lookup("panier");
		PanierClient p = panier.creerPanier();
		p.ajoutArticle(new Article("rock", "highway to hell", 2));
		//System.out.println(p.calculTotal());
//		p.detruitePanier();
//		System.out.println(p.calculTotal());
		
		PoolPanier<PanierClient> poolP = (PoolPanier<PanierClient>) registry.lookup("poolPanier");
		
		Listenner l1 = new ListernerVide(poolP);
		Listenner l2 = new ListenerSeuil(poolP);
		
		//UnicastRemoteObject.exportObject(l1, 0);
		
		poolP.addListnner((Listenner)UnicastRemoteObject.exportObject(l1, 0));
		poolP.addListnner((Listenner)UnicastRemoteObject.exportObject(l2, 0));
		
		PanierClient pc = poolP.getInstance();
		
		pc.ajoutArticle(new Article("rock", "highway to hell", 2));
		System.out.println("Total panier avec pool : " + pc.calculTotal());
		System.out.println("Nombre de paniers disponibles : " + poolP.nbObjetDispo());
		//poolP.release(pc);
		//UnicastRemoteObject.unexportObject(pc, false);
		System.out.println("Nombre de paniers disponibles : " + poolP.nbObjetDispo());
		
	}

}
