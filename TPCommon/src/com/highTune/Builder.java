package com.highTune;

import java.util.function.Supplier;

public class Builder<T> {
	private Supplier<T> supplier;

	public Builder(Supplier<T> supplier) {
		super();
		this.supplier = supplier;
	}
	
	public T getInstance(){
		return supplier.get();
	}
}
