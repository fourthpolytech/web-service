package com.highTune;

public abstract class ListennerAbstract implements Listenner{
	protected PoolPanier<PanierClient> pc;

	public ListennerAbstract(PoolPanier<PanierClient> pc) {
		super();
		this.pc = pc;
	}

	public PoolPanier<PanierClient> getPc() {
		return pc;
	}

	public void setPc(PoolPanier<PanierClient> pc) {
		this.pc = pc;
	}
	
	
}
