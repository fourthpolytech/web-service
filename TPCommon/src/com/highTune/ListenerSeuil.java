package com.highTune;

import java.rmi.RemoteException;

public class ListenerSeuil extends ListennerAbstract {

	public ListenerSeuil(PoolPanier<PanierClient> pc) {
		super(pc);
	}

	@Override
	public void update() {
		try {
			if(pc.nbObjetDispo() > 50){
				System.out.println("Listenner Seuil, le seuil est superieur a 50");
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

}
