package com.highTune;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class Panier implements IPanier{
	private TypePanier type;
	private Map<Article, Integer> produits;
	private int nombreArticle;
	
	public Panier(){
		type = new PanierNormal();
		produits = new HashMap<>();
		nombreArticle = 0;
	}
	
	public void convertirEnCommande(){
		type = new Commande();
	}
	
	/**
	 * ajouter un article avec une quantite
	 * @param a
	 * @param count
	 */
	public void ajouterArticle(Article a, int count){
		if(count < 1)
			throw new IllegalArgumentException("quantite non valide");
		//String cle = a.getCle();
		Integer val = produits.get(a);
		if(val == null){
			produits.put(a, count);
			nombreArticle += count;
		}
		else{
			produits.put(a, count+val);
		}
	}
	
	/**
	 * ajouter un seul article
	 * @param a
	 */
	public void ajouterArticle(Article a){
		ajouterArticle(a, 1);
	}
	
	/**
	 * supprimer un article
	 * @param a
	 */
	public void supprimerArticle(Article a){
		Integer val = produits.get(a);
		if(val == null){
			return ;
		}
		if(val > 1){
			produits.remove(a);
			produits.put(a, val-1);
			nombreArticle--;
		}
		else{
			produits.remove(a);
			nombreArticle--;
		}
	}
	
	/**
	 * supprimer un article avec une quantite
	 * @param a
	 * @param count
	 */
	public void supprimerArticle(Article a, int count){
		if(count < 1)
			throw new IllegalArgumentException("quantite non valide");
		for(int i = 1; i<=count; i++){
			supprimerArticle(a);
		}
	}
	/**
	 * 
	 * @return
	 */
	public int nombreTotalArticle(){
		return nombreArticle;
	}
	
	public float calculPanier(){
		float total = 0;
		Iterator<Entry<Article, Integer>> it = produits.entrySet().iterator();
		int quant;
		float prix;
		Entry<Article, Integer> ent ;
		
		while(it.hasNext()){
			ent = it.next();
			 prix= ent.getKey().getPrix();
			 quant = ent.getValue();
			 total += (prix *quant);
		}
		return total;
	}
	
	public void vider(){
		produits.clear();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + nombreArticle;
		result = prime * result + ((produits == null) ? 0 : produits.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Panier other = (Panier) obj;
		if (nombreArticle != other.nombreArticle)
			return false;
		if (produits == null) {
			if (other.produits != null)
				return false;
		} else if (!produits.equals(other.produits))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	
	public boolean isVide(){
		return produits.isEmpty();
	}

}
