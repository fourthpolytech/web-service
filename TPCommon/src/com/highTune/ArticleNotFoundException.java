package com.highTune;

public class ArticleNotFoundException extends Exception{
	private String mess;

	public ArticleNotFoundException(String mess) {
		super();
		this.mess = mess;
	}
	
}
