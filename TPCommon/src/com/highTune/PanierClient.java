package com.highTune;

import java.rmi.Remote;
import java.rmi.RemoteException;


public interface PanierClient extends Remote{
	
	public PanierClient creerPanier()throws RemoteException;
	public void validerPanier()throws RemoteException;
	public void ajoutArticle(Article a)throws RemoteException;
	public void supprimerArticle(Article a)throws RemoteException;
	public void supprimerArticle(Article a, int quantite)throws RemoteException;
	public float calculTotal()throws RemoteException;
	public void detruitePanier()throws RemoteException;
	public void viderPanier() throws RemoteException;
	public boolean isVide() throws RemoteException;
	
}
