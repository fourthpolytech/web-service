package com.highTune;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Catalogue extends Remote{
	public int nombreTotalArticle() throws RemoteException;
	public String descriptionArticle(Article a) throws RemoteException;
	public Article getArticle(String cle) throws RemoteException, ArticleNotFoundException;
}
