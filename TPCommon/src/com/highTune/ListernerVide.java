package com.highTune;

import java.rmi.RemoteException;

public class ListernerVide extends ListennerAbstract{

	public ListernerVide(PoolPanier<PanierClient> pc) {
		super(pc);
	}

	@Override
	public void update() {
		try {
			if (pc.isVide()){
				System.out.println("Listenner vide, le panier est vide");
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
