package com.highTune;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.function.Supplier;

public interface PoolPanier<T extends Remote> extends Remote{
	public void initialise()throws RemoteException;
	public int nbObjetDispo()throws RemoteException;
	public int tailleMax() throws RemoteException;
	public T getInstance()throws RemoteException;
	public void release(T obj) throws RemoteException;
	public boolean isVide() throws RemoteException;
	public void addListnner(Listenner l) throws RemoteException;
	
	public void removeListenner(Listenner l) throws RemoteException;
}
