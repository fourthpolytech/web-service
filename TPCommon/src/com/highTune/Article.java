package com.highTune;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Article implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cle;
	private String descrption;
	private float prix;
	LocalDateTime dateDispo;
	
	
	public Article(String cle, String descrption, float prix, LocalDateTime dateDispo) {
		super();
		this.cle = cle;
		this.descrption = descrption;
		this.prix = prix;
		this.dateDispo = dateDispo;
	}


	public Article(String cle, String descrption, float prix) {
		this(cle, descrption, prix, null);
		this.cle = cle;
		this.descrption = descrption;
		this.prix = prix;
	}


	public String getCle() {
		return cle;
	}


	public void setCle(String cle) {
		this.cle = cle;
	}


	public String getDescrption() {
		return descrption;
	}


	public void setDescrption(String descrption) {
		this.descrption = descrption;
	}


	public float getPrix() {
		return prix;
	}


	public void setPrix(float prix) {
		this.prix = prix;
	}


	public LocalDateTime getDateDispo() {
		return dateDispo;
	}


	public void setDateDispo(LocalDateTime dateDispo) {
		this.dateDispo = dateDispo;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cle == null) ? 0 : cle.hashCode());
		result = prime * result + ((dateDispo == null) ? 0 : dateDispo.hashCode());
		result = prime * result + ((descrption == null) ? 0 : descrption.hashCode());
		result = prime * result + Float.floatToIntBits(prix);
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Article other = (Article) obj;
		if (cle == null) {
			if (other.cle != null)
				return false;
		} else if (!cle.equals(other.cle))
			return false;
		if (dateDispo == null) {
			if (other.dateDispo != null)
				return false;
		} else if (!dateDispo.equals(other.dateDispo))
			return false;
		if (descrption == null) {
			if (other.descrption != null)
				return false;
		} else if (!descrption.equals(other.descrption))
			return false;
		if (Float.floatToIntBits(prix) != Float.floatToIntBits(other.prix))
			return false;
		return true;
	}
	
	
	
	
	
}
