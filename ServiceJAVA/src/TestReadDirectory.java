import java.io.File;

public class TestReadDirectory {
	
	public static void listFilesForFolder(String name) {
		File folder = new File(name);
	    for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            listFilesForFolder(name);
	        } else {
	        	String [] fullName = fileEntry.getName().split("\\.");
	        	String nom = fullName[0];
	            System.out.println(nom);
	        }
	    }
	}


	public static void main(String[] args) {
		listFilesForFolder("./files");
	}

}
