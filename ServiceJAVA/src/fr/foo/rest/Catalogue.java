package fr.foo.rest;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Catalogue {
	
	private static List<String> listImages = new ArrayList<>();
	
	public Catalogue(List<String> list){
		listImages = new ArrayList<>(list);
	}

	public Catalogue() {
		super();
	}


	public List<String> getListImages() {
		return listImages;
	}

	public void setListImages(List<String> listImages) {
		this.listImages = listImages;
	}
	
	public static void ajoutImage(String name){
		listImages.add(name);
	}
	
	public static void SupprmerImage(String name){
		listImages.remove(name);
	}
	
	public boolean contientImage(String name){
		return listImages.contains(name);
	}


	public static Catalogue listFilesForFolder(String name) {
		List<String> list = new ArrayList<>();
		File folder = new File(name);
	    for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            listFilesForFolder(name);
	        } else {
	        	String [] fullName = fileEntry.getName().split("\\.");
	        	String nom = fullName[0];
	            //System.out.println(nom);
	            list.add(nom);
	        }
	    }
	    
	    return new Catalogue(list);
	}
	
}
