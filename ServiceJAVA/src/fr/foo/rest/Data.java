package fr.foo.rest;

public class Data {
	private String nom;
	private double valeur;
	
	public Data(){
		super();
	}
	
	public Data(String nom, double valeur) {
		super();
		this.nom = nom;
		this.valeur = valeur;
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public double getValeur() {
		return valeur;
	}
	public void setValeur(double valeur) {
		this.valeur = valeur;
	}
	
	

}
