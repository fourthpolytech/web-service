package fr.foo.rest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

@Path("/truc")
public class MyResource {
	
	
	@GET
	@Produces("application/json")
	@Path("/datalist")
	public Catalogue getCatalogue(){
		return Catalogue.listFilesForFolder("./files");
	}
	
	@GET
	@Produces("text/plain") //text brut non formaté
	@Path("/removeImage")
	public String removeImage(@QueryParam("name") String name){
		Catalogue.SupprmerImage(name);
		return "ok";
	}
	
	
	
	
	
	
	@GET
	@Produces("text/html") //text brut non formaté
	public InputStream getIndex() throws IOException {
		
		return new FileInputStream("./htmlFiles/index2.html");
	}
	
	@POST
	@Path("/senddata")
	@Produces("application/json")
	public Data sendData(@FormParam("nom") String name, @FormParam("val") @DefaultValue("12") String val){
		return getData(name, val);
	}
	
	@GET
	@Produces("text/plain") //text brut non formaté
	@Path("/hello")
	public String sayHello(@QueryParam("nom") String name){
		
		return "Hello " + name;
	}
	
	@GET
	@Produces("image/jpg") //text brut non formaté
	@Path("/image")
	public InputStream getImage(@QueryParam("name") String name) throws IOException {
		
		return new FileInputStream("./files/" + name + ".jpg");
	}

	
	@GET
	@Produces("application/json") //text brut non formaté
	@Path("/data")
	public Data getData(@QueryParam("nom") String name, @QueryParam("val") String val){
		Data d = new Data();
		d.setNom("Hello " + name );
		d.setValeur(new Double(val));
		
		return d;
	}
	
	@GET
	@Produces("application/json")
	@Path("/login")
	public String login(@QueryParam("name") String name, @QueryParam("passwd") String pass) {
		if(name.equals("toto") && pass.equals("toto")) {
			return "You are connected";
		}
		return "error";
	}
	
	
}
