import java.util.Scanner;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.lifecycle.SingletonResourceProvider;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.foo.rest.MyResource;

public class Server {

	public static void main(String[] args) {
		String baseURL = "http://localhost:9000/";
        JAXRSServerFactoryBean sf = new JAXRSServerFactoryBean();
        sf.setProvider(new JacksonJaxbJsonProvider());
        sf.setResourceClasses(MyResource.class);
        sf.setResourceProvider(
        		MyResource.class, 
        		new SingletonResourceProvider(new MyResource())
        	);
        sf.setAddress(baseURL);
        sf.create();

		System.out.println("Saisir car+return pour stopper le serveur");	
		System.out.println("Lancer un navigateur sur l'URL:" + baseURL);	
		Scanner sc = new Scanner(System.in);
		sc.next();
		
		System.out.println("Serveur stopp� !");

	}

}
